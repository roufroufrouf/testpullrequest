//
//  Temperature.swift
//  testpullrequest
//
//  Created by Abdurrouf on 12/11/19.
//  Copyright © 2019 c2s1. All rights reserved.
//

import Foundation

class Temperature {
    var celcius: Float = 0
    var fahrenheit: Float = 0
    var kelvin: Float = 0
    
    /*
     TODO func convert from celcius to celcius
     */
    func celciusToCelcius() -> Float
    {
        return celcius
    }

    /*
     TODO func convert from fahrenheit to celcius
     */

    /*
     TODO func convert from kelvin to celcius
     */

    /*
     TODO func convert from celcius to fahrenheit
     */
    func celciusToFahrenheit() -> Float
    {
        return (celcius * 9 / 5) + 32
    }

    /*
     TODO func convert from fahrenheit to fahrenheit
     */

    /*
     TODO func convert from kelvin to fahrenheit
     */

    /*
     TODO func convert from celcius to kelvin
     */
    func celciusToKelvin() -> Float
    {
        return celcius + 273.15
    }

    /*
     TODO func convert from fahrenheit to kelvin
     */

    /*
     TODO func convert from kelvin to kelvin
     */
}
